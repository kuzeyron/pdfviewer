# pip install reportlab pdf2image
from functools import partial
from io import BytesIO
from os.path import expanduser
from threading import Thread

from pdf2image import convert_from_path

from kivy.clock import Clock
from kivy.core.image import Image as CoreImage
from kivy.factory import Factory
from kivy.lang import Builder
from kivy.properties import BooleanProperty, ListProperty, StringProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.popup import Popup

__all__ = ['PDFViewer']

Builder.load_string('''
#:import ZoomImage gestures4kivy.zoomimage.ZoomImage

<DocLoading@ModalView>:
    auto_dismiss: False
    canvas.before:
        Color:
            rgb: 1, 1, 1
        Rectangle:
            size: self.size
            pos: self.pos
    Label:
        text: "We are loading the document/s.\\nIt might take a while."

<PreviewImage@ZoomImage>:
    nocache: True

<PDFPopup>:
    auto_dismiss: False
    title: 'PDFViewer'

    BoxLayout:
        orientation: 'vertical'
        spacing: dp(5)

        RecycleView:
            bar_width: dp(15)
            data: [{'core_image': core_image} for core_image in root.images]
            scroll_timeout: 100
            scroll_type: ['bars', 'content']
            spacing: dp(20)
            viewclass: 'PreviewImage'
            canvas.before:
                Color:
                    rgb: .7, .7, .7
                Rectangle:
                    pos: self.pos
                    size: self.size

            RecycleBoxLayout:
                default_size_hint: 1, None
                default_size: dp(800), root.height / 1.2
                height: self.minimum_height
                orientation: 'vertical'
                padding: dp(20), dp(20)
                size_hint_y: None
                spacing: dp(10)

        Button:
            size_hint_y: .1
            on_release: root.dismiss()
            text: 'Close'

<PDFViewer>:

    FileChooserIconView:
        dirselect: False
        filters: ['*.pdf']
        path: root.path
        multiselect: root.multiselect
        on_selection: root.selection = self.selection
        on_submit: root.construct(root.selection)

    Button:
        size_hint_y: .1
        text: f"[ {len(root.selection)} ] selected files" \
            if root.multiselect else "Select"
        on_release: root.construct(root.selection)
        opacity: int(bool(root.selection))

''')


class PDFViewer(BoxLayout):
    multiselect = BooleanProperty(True)
    orientation = StringProperty('vertical')
    path = StringProperty('')
    selection = ListProperty([])

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.path = self.path or kwargs.get('path') or expanduser('~/')

    def construct(self, docs, *largs):
        self.loadingmessage = Factory.DocLoading()
        self.loadingmessage.open()
        Thread(
            target=self.load_contents,
            args=(docs, ),
            daemon=True
        ).start()

    def load_contents(self, docs, *largs):
        page, pages, documents = BytesIO(), [], []

        for doc in docs:
            documents.extend(convert_from_path(doc))

        for doc in documents:
            doc.save(page, format='png')
            page.seek(0)
            pages.append(CoreImage(page, ext='png'))
            page.seek(0)

        page.flush()
        Clock.schedule_once(partial(self.popups, pages), 0)

    def popups(self, pages, *largs):
        popup = PDFPopup(images=pages)
        self.loadingmessage.dismiss()
        popup.open()


class PDFPopup(Popup):
    images = ListProperty([])

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.images = kwargs.get('images', [])


if __name__ == '__main__':
    from kivy.app import App

    class PDFViewerApp(App):

        def build(self):
            return PDFViewer(path='/home/nemo/PDF/')

    PDFViewerApp().run()
