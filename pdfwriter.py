# pip install reportlab pdf2image
from io import BytesIO
from os import listdir
from os.path import dirname, join, realpath, splitext

from pdf2image import convert_from_bytes
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import cm
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfgen.canvas import Canvas

from kivy.core.image import Image as CoreImage
from kivy.lang import Builder
from kivy.logger import Logger
from kivy.properties import ListProperty, ObjectProperty, StringProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.popup import Popup

Builder.load_string('''
#:import ZoomImage gestures4kivy.zoomimage.ZoomImage

<PreviewImage@ZoomImage>:
    nocache: True

<PDFPreview>:
    auto_dismiss: False
    title: 'PDFViewer'

    BoxLayout:
        orientation: 'vertical'
        spacing: dp(5)

        RecycleView:
            bar_width: dp(15)
            data: [{'core_image': core_image} for core_image in root.images]
            scroll_timeout: 100
            scroll_type: ['bars', 'content']
            spacing: dp(20)
            viewclass: 'PreviewImage'
            canvas.before:
                Color:
                    rgb: .7, .7, .7
                Rectangle:
                    pos: self.pos
                    size: self.size

            RecycleBoxLayout:
                default_size_hint: 1, None
                default_size: dp(800), root.height / 1.3
                height: self.minimum_height
                orientation: 'vertical'
                padding: dp(20), dp(20)
                size_hint_y: None
                spacing: dp(10)

        BoxLayout:
            size_hint_y: .1

            Button:
                on_release: root.dismiss()
                text: 'Close'

            Button:
                on_release:
                    root.store()
                    root.dismiss()
                text: 'Save'


<PDFReader>:
    orientation: 'vertical'

    Button:
        on_release: root.construct_preview()
        text: 'Preview'

    Button:
        on_release: root.construct_pdf()
        text: 'Save'

''')


class PDFReader(BoxLayout):
    filename = StringProperty('invoice.pdf')
    fonts = ListProperty([])
    images = ListProperty([])
    orientation = StringProperty('vertical')
    path = dirname(realpath(__file__))
    path = StringProperty('')
    pdf = ObjectProperty(None, allownone=True)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        font_path = join(self.path, 'fonts')

        for font in listdir(font_path):
            name, extension = splitext(font)
            pdfmetrics.registerFont(TTFont(name, join(font_path, font)))
            self.fonts.append(join(font_path, font))

    def draw_pdf(self, *largs):
        """Test construction of the PDF file.
             Needs to be re-calibrated each time"""
        self.images.clear()
        folder = join(self.path, 'source')
        pdf = Canvas(join(self.path, self.filename), pagesize=A4)
        x, y = pdf._pagesize
        pdf.setTitle('Test model')
        pdf.setFont('Teko-Regular', 16)
        pdf.setFillColorRGB(0, 0, 0)
        pdf.drawString(20, y - 150, "Life of Kivy")
        pdf.drawImage(
            join(folder, 'kivy_logo.png'), x / 2 - (13 * cm / 2),
            10*cm, 13*cm, 10*cm)
        pdf.showPage()
        pdf.setFillColorRGB(0, 0, 0)
        pdf.drawString(20, y - 150, "A story about something called PDF")
        pdf.drawImage(
            join(folder, 'pdf_icon.png'),
            x / 2 - (13*cm / 2), 10*cm, 13*cm, 10*cm)
        pdf.showPage()

        return pdf

    def construct_preview(self, *largs):
        images = convert_from_bytes(self.draw_pdf().getpdfdata())
        img = BytesIO()

        for image in images:
            image.save(img, format='png')
            img.seek(0)
            self.images.append(CoreImage(img, ext='png'))
            img.seek(0)

        img.flush()
        Logger.info('Showing the preview of the PDF.')
        popup = PDFPreview(
            images=self.images,
            store=self.construct_pdf)
        popup.open()

    def construct_pdf(self, *largs):
        self.draw_pdf().save()
        Logger.info('Stored the PDF file.')


class PDFPreview(Popup):
    images = ListProperty([])
    store = ObjectProperty(None, allownone=True)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.images = kwargs.get('images', [])
        self.store = kwargs.get('store')


if __name__ == '__main__':
    from kivy.app import App

    class PDFViewerApp(App):

        def build(self):
            return PDFReader()

    PDFViewerApp().run()
